// custom north american projection centered at 50, 102
var crs = new L.Proj.CRS("Custom North America Conformal",
	"+proj=lcc +lat_1=30 +lat_2=70 +lat_0=50 +lon_0=-102 +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs ", {
		resolutions: [27781.3055626, 13890.6527813, 6945.32639065,3472.66319533, 1736.33159766, 868.165798832],
		origin: [-3605356.8490277478, 3629401.16476069],
		bounds: L.bounds([-3605356.8490277478, 3629401.16476069], [3506657.3750007018, -3482613.0592677584])
});

var	map = L.map('mapid', {
	crs: crs,
	continuousWorld: false,
	worldCopyJump: false,
	maxZoom: 5,
	minZoom: 2,
});

// Something like this for maxBounds, but certainly not this
// map.options['maxBounds'] = new L.LatLngBounds(map.unproject([-3605356.8490277478, 3629401.16476069]), map.unproject([3506657.3750007018, -3482613.0592677584]));

new L.TileLayer('https://s3.amazonaws.com/ski-maranthons-north-america-v2/{z}-{x}-{y}.jpg', {
	maxZoom: 5,
	minZoom: 2,
	continuousWorld: false,
	tms: false
}).addTo(map);

map.setView([50, -102], 3);

map.on('mousemove', function(e) {
	$("#latlon").text(e.latlng)
});

var skierIcon = L.icon({
    iconUrl: 'images/1skier.png',
    shadowUrl: 'images/1skier.png',

    iconSize:     [32, 32], // size of the icon
    shadowSize:   [0, 0], // 0'ed because we don't have a shadow
    iconAnchor:   [16, 16], // Anchor it in the middle I suppose?
    shadowAnchor: [16, 16],  // irrelevent
    popupAnchor:  [0, -14] // come out the top I suppose? This calculates from iconAnchor, looks better a bit closer in than -16
});

d3.csv("../data/data.csv", function(rows) {
	var snowfall_max = d3.max(rows, function(d) { return d.SnowFall; });
	var snowfall_scale = d3.scaleLinear()
     .domain([0, snowfall_max])
     .range([0, 50]);
    var skier_max = d3.max(rows, function(d) { return d.Participants; });
    // Could use scaleLog because the Birkie will otherwise make everything trivially small
	var skier_scale = d3.scaleLinear()
     .domain([0, skier_max])
     .range([0, 50]);
	for (var i = 0; i < rows.length; i++) {
		var data = rows[i];
		// Code to make elevation graph scale correctly
		var min_elevation_x = 100000;
    	var max_elevation_x = 0;
    	var min_elevation_y = 100000;
    	var max_elevation_y = 0;
    	var elevation_array = data["ElevationGraph"].split(" ");
    	for (var j = 0; j < elevation_array.length; j+=2) {
    		// they've all got to be numbers
    		elevation_array[j] = +elevation_array[j];
    		elevation_array[j+1] = +elevation_array[j+1];
    		if (elevation_array[j] < min_elevation_x) {
    			min_elevation_x = elevation_array[j];
    		}
    		if (elevation_array[j] > max_elevation_x) {
    			max_elevation_x = elevation_array[j];
    		}
    		if (elevation_array[j+1] < min_elevation_y) {
    			min_elevation_y = elevation_array[j+1];
    		}
    		if (elevation_array[j+1] > max_elevation_y) {
    			max_elevation_y = elevation_array[j+1];
    		}
    	}

    	var new_popup = $('<a>').attr('href',"http://" + data["Website"])
							.append(
								$('<div>').addClass('popup')
								.append(
									$('<h2>').html(data["Name"])
									)
								.append(
									$('<div>').addClass('datum').html(data["Town"] + ", " + data["State"] + " | Est. " + data["Established"])
									)
								.append(
									$('<div>').addClass('datum').html(data["Date"])
									)
								.append(
									$('<div>').addClass('datum').html(data["Distance"])
									)
								.append(
									$('<div>').addClass('datum').html(data["Website"])
									)
								.append(
									$('<div>').addClass('factoid')
										.append(
											$('<img>').addClass('race_image').attr('src',data["ImageFile"])
											)
									)
								.append(
									$('<div>').addClass('factoid_wrapper').addClass('row')
									.append(
										$('<div>').addClass('factoid')
											.append(
												$('<div>').addClass('factoid_inner_left')
												.append(
													$('<img>').addClass('infoicon').attr('src','../images/1info.png')
													)
												)
											.append(
												$('<div>').addClass('factoid_inner')
												.append(
													data["Factoid"]
													)
												)
										)
									.append(
										$('<div>').addClass('factoid').addClass('row')
											.append(
												$('<div>').addClass('factoid_inner_left')
												.append(
													$('<img>').addClass('skier').attr('src','../images/1skier.png')
													)
												)
											.append(
												$('<div>').addClass('factoid_inner')
												.append(
													data["Participants"] + " skiers"
													)
												)
										)
									.append(
										$('<div>').addClass('factoid').addClass('row')
											.append(
												$('<div>').addClass('factoid_inner_left')
												.append(
													$('<img>').addClass('snowflake').attr('src','../images/1snowflake.png')
													)
												)
											.append(
												$('<div>').addClass('factoid_inner')
												.append(
													data["SnowFall"] + " in/yr"
													)
												)
										)
									.append(
										$('<div>').addClass('factoid').addClass('row')
											.append(
												$('<div>').addClass('factoid_inner_left')
												.append(
													$('<img>').addClass('infoicon').attr('src','../images/1clock.png')
													)
												)
											.append(
												$('<div>').addClass('factoid_inner')
												.append(
													data["Record"]
													)
												)
										)
									)
								// .append(
							 // 		$('<div>').addClass('profile').attr('id',i+'graphic')
							 // 	)
								// .append(
								// 	$('<div>').addClass('profile')
								// 		.append(
								// 			$('<svg>').each(function () { $(this)[0].setAttribute('viewBox', min_elevation_x +
								// 				" " + min_elevation_y + " " + max_elevation_x +  " " + max_elevation_y) })
								// 				.attr('height','100px')
								// 				.attr('width','80%')
								// 				.append(
								// 					$('<polyline>').attr('stroke','none')
								// 						.attr('fill','white')
								// 						.attr('points',data["ElevationGraph"])
								// 					)
								// 			)
								// 		.append(
								// 			$('<div>').addClass('profile_text')
								// 				.append(
								// 					data["ElevationGain"] + " Ft. Climbing"
								// 					)
								// 			)
								// 	)
								.append(
									$('<div>')
										.append(
											$('<button>').addClass('more_button').attr('type','button')
												.append(
													'more'
													)
											)
									)
								);

		if (data["Latitude"] != '') {
			L.marker([parseFloat(data["Latitude"]), data["Longitude"]], {icon : skierIcon})
			.addTo(map)
			// new_popup is an array.. Expected behavior I guess?
			.bindPopup(new_popup[0])
		}
		// d3.xml("../images/alleyloop-110-310.svg").mimeType("image/svg+xml").get(function(error, xml) {
		//   	if (error) throw error;
		//   	var svgNode = xml.getElementsByTagName("svg")[0];
		//   	var new_div = d3.select($('#'+i+'graphic').get(0)).append("div");
		//   	new_div.node().appendChild(svgNode);
		//   	console.log(svgNode);
		//   	console.log(new_div);
		// });
	}
});

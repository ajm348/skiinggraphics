d3.csv("data.csv", function(rows) {
	var snowfall_max = d3.max(rows, function(d) { return d.SnowFall; });
	var snowfall_scale = d3.scaleLinear()
     .domain([0, snowfall_max])
     .range([0, 50]);
    var skier_max = d3.max(rows, function(d) { return d.Participants; });
    // Could use scaleLog because the Birkie will otherwise make everything trivially small
	var skier_scale = d3.scaleLinear()
     .domain([0, skier_max])
     .range([0, 50]);
	for (var i = 0; i < rows.length; i++) {
		var data = rows[i];
		// Code to make elevation graph scale correctly
		var min_elevation_x = 100000;
    	var max_elevation_x = 0; 
    	var min_elevation_y = 100000;
    	var max_elevation_y = 0;
    	var elevation_array = data["ElevationGraph"].split(" ");
    	for (var j = 0; j < elevation_array.length; j+=2) {
    		// they've all got to be numbers
    		elevation_array[j] = +elevation_array[j];
    		elevation_array[j+1] = +elevation_array[j+1];
    		if (elevation_array[j] < min_elevation_x) {
    			min_elevation_x = elevation_array[j];
    		}
    		if (elevation_array[j] > max_elevation_x) {
    			max_elevation_x = elevation_array[j];
    		}
    		if (elevation_array[j+1] < min_elevation_y) {
    			min_elevation_y = elevation_array[j+1];
    		}
    		if (elevation_array[j+1] > max_elevation_y) {
    			max_elevation_y = elevation_array[j+1];
    		}	
    	}
		var arbitrary_html = "<a href=http://" + data["Website"]+ "> \
								<div class='popup'> \
									<h2> \
										" + data["Name"] + " \
									</h2> \
									<div class='datum'> \
										" + data["Town"] + ", " + data["State"] + " | Est. " + data["Established"] + " \
									</div> \
									<div class='datum'> \
										" + data["Date"] + " \
									</div> \
									<div class='datum'> \
										" + data["Distance"] + " \
									</div> \
									<div class='datum link'> \
										" + data["Website"] + " \
									</div> \
									<div class='factoid'> \
										<img class='race_image' src='" + data["ImageFile"] + "' /> \
									</div> \
									<div class='factoid'> \
											<img class='infoicon' src='infoicon.png' /> \
											" + data["Factoid"] + " \
									</div> \
									<div class='factoid'> \
										<div class='snowdepth_mask' style='width:" + (skier_scale(data["Participants"]) > 10 ? skier_scale(data["Participants"]) : 10) + "%'> \
											<img class='snowflake' src='skier.png'/><img class='snowflake' src='skier.png'/><img class='snowflake' src='skier.png'/><img class='snowflake' src='skier.png'/><img class='snowflake' src='skier.png'/> \
										</div> \
										" + data["Participants"] + " skiers \
									</div> \
									<div class='factoid' id='snowdepth" + i + "'> \
										<div class='snowdepth_mask' style='width:" + snowfall_scale(data["SnowFall"]) +"%'> \
											<img class='snowflake' src='snowflake.png'/><img class='snowflake' src='snowflake.png'/><img class='snowflake' src='snowflake.png'/><img class='snowflake' src='snowflake.png'/><img class='snowflake' src='snowflake.png'/> \
										</div> \
										" + data["SnowFall"] + " in/yr \
									</div> \
									<div class='factoid'> \
											<img class='infoicon' src='stopwatch.png' /> \
											" + data["Record"] + " \
									</div> \
									<div class='profile'> \
										<svg width='80%' height='100px' viewBox='" + min_elevation_x + " " + min_elevation_y + " " + max_elevation_x +  " " + max_elevation_y + "'> \
											<polyline stroke='none' style='fill: white;' points='" + data["ElevationGraph"] + "'/> \
											<!-- <text x='20' y='75' style='fill:#4A85BC'> \
												" + data["ElevationGain"] + " Ft. Climbing \
											</text> -->\
										</svg> \
										<div class='profile_text'>" + data["ElevationGain"] + " Ft. Climbing</div>\
									</div> \
									<div> \
										<button type='button' class='more_button'>more</button> \
									</div> \
								</div> \
							</a>";
		if (data["Latitude"] != '') {
			L.marker([parseFloat(data["Latitude"]), data["Longitude"]])
			.addTo(map)
		    .bindPopup(arbitrary_html);
		}


	}
});